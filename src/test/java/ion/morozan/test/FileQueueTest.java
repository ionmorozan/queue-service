package ion.morozan.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import ion.morozan.message.IMessage;
import ion.morozan.message.Message;
import ion.morozan.queueservice.FileQueueService;
import ion.morozan.queueservice.QueueService;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

/**
 * Test the functionality and correctness of the {@link FileQueueService } class.
 * 
 * @author Ion Morozan
 */
public class FileQueueTest {
	/**
	 * Mocked version of the in memory queue
	 */
    @Mock
    private FileQueueService mockedQueue;
    
    /**
     * In memory queue service implementation
     */
    private FileQueueService queue;
    
    /**
     * Temporary file which stores the messages from the queue
     */
    private File temporaryQueueStore;
    
    /**
     * Method called on initialization.
     */
    @Before
    public void setUp()
    {
       MockitoAnnotations.initMocks(this);
       
       try {
    	   temporaryQueueStore = File.createTempFile( "TestDb", "txt");
       } catch (IOException e) {
    	   e.printStackTrace();
       }
       this.queue = new FileQueueService(temporaryQueueStore);
    }
    
    /**
     * Cleanup the queue after each test.
     */
    @After
    public void tearDown() {
        this.temporaryQueueStore.deleteOnExit();
    }
    
    /**
     * Require that the testing objects are instantiated.
     */
    @Test
    public void requireThatPropertiesAreInstantiated(){
    	// Arrange, Act & Assert
        assertNotNull(this.queue);
        assertNotNull(this.mockedQueue);
    }
    
    /**
     * Require that pushing two messages the size of the queue is increased.
     * 
     * @throws InterruptedException when thread's activity is interrupted
     */
    @Test
    public void requireThatPushIncreasesTheSizeOfTheQueue() throws InterruptedException{
    	//Arrange
    	IMessage message1 = new Message("id1", 0, "Hello World");
    	IMessage message2 = new Message("id2", 0, "Hello World2");
    	
    	// Act
        this.queue.push(message1);
        this.queue.push(message2);
        
        //Assert
        assertFalse(this.queue.isQueueEmpty());
    }
    
    /**
     * Require that pushing a null message throws NullPointerException.
     * 
     * @throws InterruptedException when thread's activity is interrupted
     */
    @Test(expected=NullPointerException.class)
    public void requireThatPushOfNullMessageThrowsNullPointerException() throws InterruptedException{
    	// Arrange, Act & Assert
        this.queue.push(null);
    }
    
    /**
     * Require that when pull is called the logical size of the queue is decreased 
     * because we make the message invisible but the physical size does not.
     *  
     * @throws InterruptedException when thread's activity is interrupted
     */
    @Test
    public void requireThatPullDecreasesTheSizeOfTheQueueButDoesNotRemoveTheMessage() throws InterruptedException{
    	//Arrange
    	IMessage message1 = new Message("id1", 0, "Hello World");
    	IMessage message2 = new Message("id2", 0, "Hello World2");
    	long expectedQueueSize = message1.serialize().length + message2.serialize().length + 2 * Integer.BYTES;
    	
    	// Act
        this.queue.push(message1);
        this.queue.push(message2);
        this.queue.pull();

        //Assert
        assertEquals(1, logicalQueueSize(this.queue));
        assertEquals(expectedQueueSize, this.temporaryQueueStore.length());
    }
 
    /**
     * Require that deletion of a message makes it inaccessible for 
     * an indefinite period of time.
     * 
     * @throws IOException an I/O exception of some sort has occurred
     */
    @Test
    public void requireThatDeleteLogicallyRemovesTheMessagesFromTheQueue() throws  IOException{
    	//Arrange
    	IMessage message = new Message("id1", 0, "Hello World");
    	int messageSize = message.serialize().length;
    	byte[] messageSizeByteArray = ByteBuffer.allocate(Integer.BYTES).putInt(messageSize).array();
    	byte[] zeroFilledMessageStream = new byte[messageSize];
        
    	// Create a tuple containing (messageSize, new byte[messageSize]) 
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream( );
        outputStream.write(messageSizeByteArray);
        outputStream.write(zeroFilledMessageStream);
        byte[] expectedContent = outputStream.toByteArray();
        
    	// Act
    	this.queue.push(message);
        this.queue.delete(message);
        byte [] actualContent = Files.readAllBytes(Paths.get(this.temporaryQueueStore.getPath())); 
        
        //Assert
        assertTrue(Arrays.equals(expectedContent, actualContent));
    }
    
    /**
     * Require that deleting a null message throws NullPointerException
     */
    @Test(expected=NullPointerException.class)
    public void requireThatDeleteOfNullMessageThrowsNullPointerException(){
    	//Arrange
    	IMessage message = new Message("id1", 0, "Hello World");
    	
    	// Act
    	this.queue.push(message);
        this.queue.delete(null);
    }
    
    /**
     * Require that pull makes the message invisible so the logical size of the queue is decreased.
     * 
     * @throws InterruptedException when thread's activity is interrupted
     */
    @Test
    public void requireThatPullMakesTheMessageInvisible() throws InterruptedException{
    	//Arrange
    	IMessage message1 = new Message("id1", 0, "Hello World");
    	IMessage mockedMessage = Mockito.mock(Message.class);
    	when(mockedMessage.serialize()).thenReturn(message1.serialize());
    	
    	// Act
        this.queue.push(mockedMessage);
        this.queue.pull();
        
        //Assert
        verify(mockedMessage, Mockito.atMost(1)).isVisible();
        verify(mockedMessage, Mockito.atMost(1)).setRandomVisibilityTimeout();
        assertEquals(0, logicalQueueSize(this.queue));
    }
    
    /**
     * Require that that push stores the expected message.
     * 
     * @throws InterruptedException when thread's activity is interrupted
     */
    @Test
    public void requireThatPushStoresExpectedMessage() throws InterruptedException{
    	//Arrange
    	IMessage actualMessage = new Message("id1", 0, "Hello World");
    	
    	// Act
        this.queue.push(actualMessage);
        IMessage expectedMessage = this.queue.pull();
        
        //Assert
        assertEquals(expectedMessage.getBody(), actualMessage.getBody());
        assertEquals(expectedMessage.getId(), actualMessage.getId());
    }
    
    /**
     * Require that the messages are stored in FIFO (first in first out) order in the queue.
     * 
     * @throws InterruptedException when thread's activity is interrupted
     */
    @Test
    public void requireThatFIFOOrderIsRespected() throws InterruptedException{
    	//Arrange
    	IMessage actualMessage1 = new Message("id1", 0, "Hello World");
    	IMessage actualMessage2 = new Message("id2", 0, "Hello World2");
    	
    	// Act
        this.queue.push(actualMessage1);
        this.queue.push(actualMessage2);
        
        IMessage expectedFirstMessage = this.queue.pull();
        IMessage expectedSecondMessage = this.queue.pull();
        
        //Assert
        assertEquals(expectedFirstMessage.getId(), actualMessage1.getId());
        assertEquals(expectedFirstMessage.getBody(), actualMessage1.getBody());
        assertEquals(expectedSecondMessage.getId(), actualMessage2.getId());
        assertEquals(expectedSecondMessage.getBody(), actualMessage2.getBody());
    }
    
    /**
     * Require that queue is empty when no message was added
     */
    @Test
    public void requireThatQueueIsEmptyWhenNoMessageWasAdded(){
    	// Arrange, Act, Assert
    	assertTrue(this.queue.isQueueEmpty());
    }
    
    /**
     * Helper method to get the logical size of the queue.
     * Count all messages that are visible.
     * 
     * @param queue - resource to count the messages
     * @return the number of visible messages in the queue
     * @throws InterruptedException when thread's activity is interrupted
     */
    private int logicalQueueSize(QueueService queue) throws InterruptedException{
    	int queueSize = 0;
    	while (queue.pull() != null){
    		queueSize++;
    	}
    	
    	return queueSize;
    }
}