package ion.morozan.test;

import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.Test;
import org.junit.Before;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.*;
import ion.morozan.message.IMessage;
import ion.morozan.message.Message;
import ion.morozan.queueservice.InMemoryQueueService;

/**
 * Test the functionality and correctness of the {@link Message} class. 
 * 
 * @author Ion Morozan
 */
public class MessageTest {

	/**
	 * Mocked version of the in memory queue
	 */
    @Mock
    private InMemoryQueueService mockedQueue;
    
	/**
	 * Mocked version of the message
	 */
    @Mock
    private Message mockedMessage;
    
    /**
     * Message stored in the queue
     */
    private Message message;
    
    /**
     * Method called on initialization.
     */
    @Before
    public void setUp()
    {
       MockitoAnnotations.initMocks(this);
       this.message = new Message("id1", 0, "Hello World");
    }
	
    /**
     * Require that the testing objects are instantiated.
     */
    @Test
    public void requireThatPropertiesAreInstantiated(){
    	// Arrange, Act & Assert
        assertNotNull(this.mockedMessage);
    }
    
    /**
     * Require that message is visible after it is created.
     */
    @Test
    public void requireThatMessageIsVisibleAfterItsCreated(){
        // Arrange, Act & Assert
        assertTrue(message.isVisible());
    }
    
    /**
     * Require that message is not visible once the visibility timeout was called.
     */
    @Test
    public void requireThatMessageIsNotVisibleAfterTheTimeOutWasSet(){
    	// Arrange & Act
    	message.setRandomVisibilityTimeout();
    	
        // Assert
        assertFalse(message.isVisible());
    }

    /**
     * Require that set random visibility timeout is called when pulling a message from a queue.
     * 
     * @throws InterruptedException when thread's activity is interrupted
     */
    @Test
    public void requireThatSetRandomVisibilityTimeoutIsCalledWhenPullingAMessageToQueue() throws InterruptedException{
    	
    	// Arrange & Act
    	this.mockedQueue.push(this.mockedMessage);
    	this.mockedQueue.pull();
    	
        // Assert
        verify(this.mockedMessage, atMost(1)).setRandomVisibilityTimeout();
    }
    
    /**
     * Require that a message is not null after calling create.
     */
    @Test
    public void requireThatMessageIsNotNullAfterCallingCreateMessage(){
    	// Arrange & Act
    	IMessage newMessage = new Message().createMessage();
    	
        // Assert
        assertNotNull(newMessage);
    }
    
    /**
     * Require that message body is not null once the message it is created
     */
    @Test
    public void requireThatMessageBodyIsNotNullOnceCreated(){
    	// Arrange, Act & Assert
        assertNotNull(this.message.getBody());
    }
    
    /**
     * Require that message body changes after is set.
     */
    @Test
    public void requireThatMessageBodyChangedAfterSetBeinCalled(){
    	// Arrange
    	String expectedBody = "New message body.";
    	
    	// Act
    	this.message.setBody(expectedBody);
    	
        // Assert
        assertEquals(expectedBody, this.message.getBody());
    }
    
    /**
     * Require that message is not null once it is created.
     */
    @Test
    public void requireThatMessageIdIsNotNullOnceCreated(){
    	// Arrange, Act & Assert
        assertNotNull(this.message.getId());
    }
    
    /**
     * Require that the message id is change once it is set.
     */
    @Test
    public void requireThatMessageIdChangedAfterSetBeinCalled(){
    	// Arrange
    	String expectedId = "NewId";
    	
    	// Act
    	this.message.setId(expectedId);
    	
        // Assert
        assertEquals(expectedId, this.message.getId());
    }
    
    /**
     * Require that the serializer produces the expected byte stream.
     */
    @Test
    public void requireThatSerializeProducesTheExpectedByteStream(){
    	// Arrange
    	byte[] expectedMessageStream = new byte[] {-84, -19, 0, 5, 115, 114, 0, 27, 105, 111, 
    			110, 46, 109, 111, 114, 111, 122, 97, 110, 46, 109, 101, 115, 115, 97, 103, 
    			101, 46, 77, 101, 115, 115, 97, 103, 101, 52, -120, -82, 46, -81, 118, -78, 
    			-115, 2, 0, 3, 74, 0, 17, 118, 105, 115, 105, 98, 105, 108, 105, 116, 121, 
    			84, 105, 109, 101, 111, 117, 116, 76, 0, 4, 98, 111, 100, 121, 116, 0, 18, 
    			76, 106, 97, 118, 97, 47, 108, 97, 110, 103, 47, 79, 98, 106, 101, 99, 116, 
    			59, 76, 0, 2, 105, 100, 116, 0, 18, 76, 106, 97, 118, 97, 47, 108, 97, 110, 
    			103, 47, 83, 116, 114, 105, 110, 103, 59, 120, 112, 0, 0, 0, 0, 0, 0, 0, 0, 
    			116, 0, 11, 72, 101, 108, 108, 111, 32, 87, 111, 114, 108, 100, 116, 0, 3, 
    			105, 100, 49};
    			
    	// Act
    	byte[] actualMessageStream = this.message.serialize();
    	
        // Assert
        assertTrue(Arrays.equals(expectedMessageStream, actualMessageStream));
    }
    
    /**
     * Require that the deserializer produces the expected message.
     */
    @Test
    public void requireThatDeserializeProducesTheExpectedMessage(){
    	// Arrange
    	byte[] messageStream = new byte[] {-84, -19, 0, 5, 115, 114, 0, 27, 105, 111, 
    			110, 46, 109, 111, 114, 111, 122, 97, 110, 46, 109, 101, 115, 115, 97, 103, 
    			101, 46, 77, 101, 115, 115, 97, 103, 101, 52, -120, -82, 46, -81, 118, -78, 
    			-115, 2, 0, 3, 74, 0, 17, 118, 105, 115, 105, 98, 105, 108, 105, 116, 121, 
    			84, 105, 109, 101, 111, 117, 116, 76, 0, 4, 98, 111, 100, 121, 116, 0, 18, 
    			76, 106, 97, 118, 97, 47, 108, 97, 110, 103, 47, 79, 98, 106, 101, 99, 116, 
    			59, 76, 0, 2, 105, 100, 116, 0, 18, 76, 106, 97, 118, 97, 47, 108, 97, 110, 
    			103, 47, 83, 116, 114, 105, 110, 103, 59, 120, 112, 0, 0, 0, 0, 0, 0, 0, 0, 
    			116, 0, 11, 72, 101, 108, 108, 111, 32, 87, 111, 114, 108, 100, 116, 0, 3, 
    			105, 100, 49};
    	
    	// Act
    	IMessage expectedMessage = Message.deserialize(messageStream);
    	
        // Assert
        assertEquals(expectedMessage, this.message);
    }
    
    /**
     * Require that comparing two equal message return true.
     */
    @Test
    public void requireThatEqualsReturnTrueWhenComparingtheSameMessages(){
    	// Arrange
    	IMessage sameMessage = new Message("id1", 0, "Hello World");
    	
        // Act & Assert
        assertTrue(this.message.equals(sameMessage));
    }
    
    /**
     * Require that toString() produces the expected output.
     */
    @Test
    public void requireThatToStringProducesTheExpectedOutput(){
    	// Arrange
    	String expectedString = "Message with id: id1 by main";
    	
        // Act & Assert
        assertEquals(expectedString, this.message.toString());
    }
    
    /**
     * Require that an empty byte array return true when checking if all bytes are zero.
     */
    @Test
    public void requireThatAnEmptyByteArrayReturnsTrueWhenCallingIsMessageStreamZeroFilled(){
    	// Arrange
    	byte[] emptyByteArray = new byte[100];
    			
        // Act & Assert
        assertTrue(Message.isMessageStreamZeroFilled(emptyByteArray));
    }
    
    /**
     * Require that a non-empty byte array return false when checking if all bytes are zero.
     */
    @Test
    public void requireThatAnNonEmptyByteArrayReturnsFalseWhenCallingIsMessageStreamZeroFilled(){
    	// Arrange
    	byte[] emptyByteArray = new byte[] {0, 0, 1, 3};
    			
        // Act & Assert
        assertFalse(Message.isMessageStreamZeroFilled(emptyByteArray));
    }
    
    /**
     * Require that base64 encode produces the expected output.
     */
    @Test
    public void requireThatBase64EncodeProducesTheExpectedOutput(){
    	// Arrange
    	String expectedBase64Output = "rO0ABXNyABtpb24ubW9yb3phbi5tZXNzYWdlLk1lc3NhZ2U0iK4ur3ay"
    			+ "jQIAA0oAEXZpc2liaWxpdHlUaW1lb3V0TAAEYm9keXQAEkxqYXZhL2xhbmcvT2JqZWN0O0wAAmlk"
    			+ "dAASTGphdmEvbGFuZy9TdHJpbmc7eHAAAAAAAAAAAHQAC0hlbGxvIFdvcmxkdAADaWQx";

    	// Act
    	String actualBase64Output = Message.encodeToBase64(this.message);

        // Assert
        assertEquals(expectedBase64Output, actualBase64Output);
    }
    
    /**
     * Require that base64 decode produces the expected message.
     */
    @Test
    public void requireThatBase64DecodeProducesTheExpectedMessage(){
    	// Arrange
    	String base64EcodedMessage = "rO0ABXNyABtpb24ubW9yb3phbi5tZXNzYWdlLk1lc3NhZ2U0iK4ur3ay"
    			+ "jQIAA0oAEXZpc2liaWxpdHlUaW1lb3V0TAAEYm9keXQAEkxqYXZhL2xhbmcvT2JqZWN0O0wAAmlk"
    			+ "dAASTGphdmEvbGFuZy9TdHJpbmc7eHAAAAAAAAAAAHQAC0hlbGxvIFdvcmxkdAADaWQx";

    	// Act
    	IMessage expectedMessage = Message.decodeFromBase64(base64EcodedMessage);
    			
        // Assert
        assertEquals(expectedMessage, this.message);
    }
}