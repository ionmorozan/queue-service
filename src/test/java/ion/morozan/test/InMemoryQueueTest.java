package ion.morozan.test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import static org.mockito.Mockito.*;

import ion.morozan.message.IMessage;
import ion.morozan.message.Message;
import ion.morozan.queueservice.InMemoryQueueService;
import ion.morozan.queueservice.QueueService;

/**
 * Test the functionality and correctness of the {@link InMemoryQueueService } class.
 * 
 * @author Ion Morozan
 */
public class InMemoryQueueTest{

	/**
	 * Mocked version of the in memory queue
	 */
    @Mock
    private InMemoryQueueService mockedQueue;
    
    /**
     * In memory queue service implementation
     */
    private InMemoryQueueService queue;
    
    /**
     * Method called on initialization.
     */
    @Before
    public void setUp()
    {
       MockitoAnnotations.initMocks(this);
       this.queue = new InMemoryQueueService();
    }
  
    /**
     * Require that the testing objects are instantiated.
     */
    @Test
    public void requireThatPropertiesAreInstantiated(){
    	// Arrange, Act & Assert
        assertNotNull(this.queue);
        assertNotNull(this.mockedQueue);
    }
    
    /**
     * Require that pushing two messages the size of the queue is increased.
     * 
     * @throws InterruptedException when thread's activity is interrupted
     */
    @Test
    public void requireThatPushIncreasesTheSizeOfTheQueue() throws InterruptedException{
    	//Arrange
    	IMessage message1 = new Message("id1", 0, "Hello World");
    	IMessage message2 = new Message("id2", 0, "Hello World2");
    	
    	// Act
        this.queue.push(message1);
        this.queue.push(message2);
        
        //Assert
        assertEquals(2, this.queue.getQueueSize());
    }
    
    /**
     * Require that pushing a null message throws NullPointerException.
     * 
     * @throws InterruptedException when thread's activity is interrupted
     */
    @Test(expected=NullPointerException.class)
    public void requireThatPushOfNullMessageThrowsNullPointerException() throws InterruptedException{
    	// Arrange, Act & Assert
        this.queue.push(null);
    }
    
    /**
     * Require that when pull is called the logical size of the queue is decreased 
     * because we make the message invisible but the physical size does not.
     *  
     * @throws InterruptedException when thread's activity is interrupted
     */
    @Test
    public void requireThatPullDecreasesTheSizeOfTheQueueButDoesNotRemoveTheMessage() throws InterruptedException{
    	//Arrange
    	IMessage message1 = new Message("id1", 0, "Hello World");
    	IMessage message2 = new Message("id2", 0, "Hello World2");
    	
    	// Act
        this.queue.push(message1);
        this.queue.push(message2);
        this.queue.pull();
        
        //Assert
        assertEquals(1, queueSize(this.queue));
        assertEquals(2, this.queue.getQueueSize());
    }
    
    /**
     * Require that pull on a message makes it invisible for a 
     * certain time, but then it becomes again available at the head.
     * We test this by adding 2 messages, pulling the head, then 
     * mocking the invisible head to act as the visibility timeout expired.
     * 
     * @throws InterruptedException when thread's activity is interrupted
     */
    @Test
    public void requireThatPullMakesTheMessageInvisibleForCertainTimeButDoesNotRemoveTheMessage() throws InterruptedException{
    	//Arrange
    	IMessage message1 = Mockito.mock(Message.class);
    	IMessage message2 =  Mockito.mock(Message.class);
    	
    	// Act
        this.queue.push(message1);
        this.queue.push(message2);
        this.queue.pull();
        when(message1.isVisible()).thenReturn(false);
        when(message2.isVisible()).thenReturn(true, false, true, false);
        
        //Assert
        assertEquals(1, queueSize(this.queue));
        when(message1.isVisible()).thenReturn(true, false);
        assertEquals(2, queueSize(this.queue));
        assertEquals(2, this.queue.getQueueSize());
    }
    
    /**
     * Require that deletion of a message decreases the size of the queue.
     * 
     * @throws InterruptedException when thread's activity is interrupted
     */
    @Test
    public void requireThatDeleteRemovesTheMessagesFromTheQueue() throws InterruptedException{
    	//Arrange
    	IMessage message = Mockito.mock(Message.class);
    	
    	// Act
    	this.queue.push(message);
        this.queue.delete(message);
        
        //Assert
        assertEquals(0, this.queue.getQueueSize());
    }
    
    /**
     * Require that deleting a null message throws NullPointerException
     * 
     * @throws InterruptedException when thread's activity is interrupted
     */
    @Test(expected=NullPointerException.class)
    public void requireThatDeleteOfNullMessageThrowsNullPointerException() throws InterruptedException{
    	//Arrange
    	IMessage message = Mockito.mock(Message.class);
    	
    	// Act
    	this.queue.push(message);
        this.queue.delete(null);
    }
    
    /**
     * Require that pull makes the message invisible so the logical size of the queue is decreased.
     * 
     * @throws InterruptedException when thread's activity is interrupted
     */
    @Test
    public void requireThatPullMakesTheMessageInvisible() throws InterruptedException{
    	//Arrange
    	IMessage message1 = Mockito.mock(Message.class);
    	
    	// Act
        this.queue.push(message1);
        this.queue.pull();
        
        //Assert
        verify(message1, Mockito.atMost(1)).isVisible();
        verify(message1, Mockito.atMost(1)).setRandomVisibilityTimeout();
        assertEquals(0, queueSize(this.queue));
    }
   
    /**
     * Require that that push stores the expected message.
     * 
     * @throws InterruptedException when thread's activity is interrupted
     */
    @Test
    public void requireThatPushStoresExpectedMessage() throws InterruptedException{
    	//Arrange
    	IMessage actualMessage = new Message("id1", 0, "Hello World");
    	
    	// Act
        this.queue.push(actualMessage);
        IMessage expectedMessage = this.queue.pull();
        
        //Assert
        assertEquals(expectedMessage, actualMessage);
    }
    
    /**
     * Require that the messages are stored in FIFO (first in first out) order in the queue.
     * 
     * @throws InterruptedException when thread's activity is interrupted
     */
    @Test
    public void requireThatFIFOOrderIsRespected() throws InterruptedException{
    	//Arrange
    	IMessage message1 = new Message("id1", 0, "Hello World");
    	IMessage message2 = new Message("id2", 0, "Hello World2");
    	
    	// Act
        this.queue.push(message1);
        this.queue.push(message2);
        
        IMessage expectedFirstMessage = this.queue.pull();
        IMessage expectedSecondMessage = this.queue.pull();
        
        //Assert
        assertEquals(message1, expectedFirstMessage);
        assertEquals(message2, expectedSecondMessage);
    }
    
    /**
     * Helper method to get the logical size of the queue.
     * Count all messages that are visible.
     * 
     * @param queue - resource to count the messages
     * @return the number of visible messages in the queue
     * @throws InterruptedException when thread's activity is interrupted
     */
    private int queueSize(QueueService queue) throws InterruptedException{
    	int queueSize = 0;
    	while (queue.pull() != null){
    		queueSize++;
    	}
    	
    	return queueSize;
    }
}