package ion.morozan.message;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Base64;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Definition of a message stored in a queue.
 * 
 * @author Ion Morozan
 */
public class Message implements IMessage, Serializable{
	
	/**
	 * The unique identifier of the message
	 */
	private String id;
	
	/**
	 * Timeout where the message is not visible in the queue
	 */
	private long visibilityTimeout;
	
	/**
	 * Content of the message
	 */
	private Object body;
	
	/**
	 * Id used to validate the deserialization process
	 */
	private static final long serialVersionUID = 3785467002340422285L;
	
	/**
	 * Default invisibility timeout.
	 */
	private static final long DEFAULT_TIMEOUT = 0;
	
	/**
	 * Max invisibility timeout in seconds.
	 */
	public static final int MAX_TIMEOUT = 10;
	
	/**
	 * Min invisibility timeout in seconds.
	 */
	public static final int MIN_TIMEOUT = 1;
	
	public Message(){};
	
	/**
	 * Get an instance of the {@link Message} class.
	 * 
	 * @param id - unique identifier of the message
	 * @param visibilityTimeout - timeout where the message is not visible in the queue
	 * @param body - content of the message
	 */
	public Message(String id, long visibilityTimeout, Object body){
		this.id = id;
		this.visibilityTimeout = visibilityTimeout;
		this.body = body;
	}

	/**
	 * {@inheritDoc} 
	 */	
	@Override
	public IMessage createMessage() {
		String id = UUID.randomUUID().toString();
		
		return new Message(id, DEFAULT_TIMEOUT, id.toString());
	}

	/**
	 * {@inheritDoc} 
	 */	
	@Override
	public boolean isVisible() {
		if (this.visibilityTimeout - System.currentTimeMillis() < 0){
			return true;
		}
		System.out.println("\t[Info] Message is not visible. Skipping...");
		return false;
	}
	
	/**
	 * {@inheritDoc} 
	 */	
	@Override
	public void setRandomVisibilityTimeout() {
		this.visibilityTimeout = System.currentTimeMillis() + (1000 * ThreadLocalRandom.current().nextLong(MIN_TIMEOUT, MAX_TIMEOUT));
		System.out.println("\t[Info] Now message is invisibile for: " + (this.visibilityTimeout - System.currentTimeMillis()) + "ms");
	}

	/**
	 * {@inheritDoc} 
	 */	
	@Override
	public Object getBody() {
		return this.body;
	}

	/**
	 * {@inheritDoc} 
	 */	
	public void setBody(Object message) {
		this.body = message;
	}

	/**
	 * {@inheritDoc} 
	 */	
	@Override
	public String getId() {
		return this.id;
	}
	
	/**
	 * {@inheritDoc} 
	 */	
	@Override
	public void setId(String id) {
		this.id = id;
	}
	
	/**
	 * {@inheritDoc} 
	 */	
	@Override
	public byte[] serialize(){
		byte[] messageStream  = null; 
		
		try(ByteArrayOutputStream bos = new ByteArrayOutputStream()){
			ObjectOutput stream;
			stream = new ObjectOutputStream(bos);
			stream.writeObject(this);
			stream.flush();
			
			messageStream = bos.toByteArray(); 
		} catch (IOException e) {
			e.printStackTrace();
		} 
		
		return messageStream; 
	}

	/**
	 * Format a message to be displayed. 
	 */	
	@Override
	public String toString(){
		return  "Message with id: " + this.id.substring(Math.max(0, this.id.length()-35)) + " by " + Thread.currentThread().getName();
	}

	/**
	 * Check if two messages are the same.
	 */
	@Override
    public boolean equals(Object o) {
        if (o instanceof Message) {
            Message messageToCompare = (Message) o;
            return this.id.equals(messageToCompare.id)
                    && this.body.equals(messageToCompare.body)
                    && this.visibilityTimeout == messageToCompare.visibilityTimeout;
        }
        return false;
    }
	
	/**
	 * Check if a serialized message is null.
	 * 
	 * @param messageStream - binary representation of the message
	 * @return true if the message is null, false otherwise
	 */
	public static boolean isMessageStreamZeroFilled(byte[] messageStream){
		for (byte b : messageStream) {
		    if (b != 0) {
		        return false;
		    }
		}
		return true;
	}
	
	/**
	 * Encode a message to Base64.
	 * 
	 * @param message - queue message to be encoded
	 * @return the message Base64 encoded
	 */
	public static String encodeToBase64(IMessage message){
		return Base64.getEncoder().encodeToString(message.serialize());
	}
	
	/**
	 * Decode a message from Base64.
	 * 
	 * @param encodedMessage - message encoded in Base64
	 * @return an instance of the message
	 */
	public static IMessage decodeFromBase64(String encodedMessage){
		byte[] messageStream = Base64.getDecoder().decode(encodedMessage);
		
		return Message.deserialize(messageStream);
	}
	
	/**
	 * Try to deserialize a sequence of bytes to an instance of a message.
	 * 
	 * @param messageStream - sequence of bytes
	 * @return an instance of the message or null otherwise.
	 */
	public static IMessage deserialize(byte[] messageStream) {
		IMessage message = null;
		
		// Check if the message is null
		if (Message.isMessageStreamZeroFilled(messageStream)){
			return null;
		}
		
		// Proceed with the deserialization
		ByteArrayInputStream bis = new ByteArrayInputStream(messageStream);
		try(ObjectInput in = new ObjectInputStream(bis)){
			message = (IMessage)in.readObject();
		} 
		catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return message;
	}
}