package ion.morozan.message;

/**
 * Interface which defines the message from a queue.
 * 
 * @author Ion Morozan
 */
public interface IMessage{

	/**
	 * Generate a random message.
	 * 
	 * @return the message created
	 */
	public IMessage createMessage();
	
	/**
	 * Check if the message is visible.
	 * 
	 * @return true if the message is visible false otherwise.
	 */
	public boolean isVisible();
	
	/**
	 * Set a random invisibility timeout for a message 
	 * in the queue in milliseconds.
	 */
	public void setRandomVisibilityTimeout();
	
	/**
	 * Serialize a message to binary format.
	 * 
	 * @return the message as a stream of bytes 
	 */
	public byte[] serialize();
	
	/**
	 * Get the body of a message.
	 * 
	 * @return the body of the message
	 */
	public Object getBody();
	
	/**
	 * Get the id of the message.
	 * 
	 * @return the id of the message
	 */
	public String getId();
	
	/**
	 * Set the id to a message.
	 * 
	 * @param id - identifier to be set
	 */
	public void setId(String id);
}