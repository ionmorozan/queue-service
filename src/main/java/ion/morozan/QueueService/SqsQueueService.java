package ion.morozan.queueservice;

import ion.morozan.message.IMessage;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.sqs.AmazonSQSClient;
import com.amazonaws.regions.Region;
import com.amazonaws.services.sqs.model.ChangeMessageVisibilityRequest;
import com.amazonaws.services.sqs.model.CreateQueueRequest;
import com.amazonaws.services.sqs.model.DeleteMessageRequest;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.ReceiveMessageRequest;
import com.amazonaws.services.sqs.model.SendMessageRequest;

/**
 * Class handling an Amazon Simple Queue Service (SQS) implementation.
 * 
 * @author Ion Morozan
 */
public class SqsQueueService implements QueueService {

	/**
	 * Constructs a new client to invoke service methods on Amazon SQS.
	 */
	private final AmazonSQSClient sqsClient;
	
	/**
	 * Handler to the Amazon SQS queue.
	 */
	private final String queue;
	
	/**
	 * Create an instance of the {@link SqsQueueService} class.
	 * 
	 * @param sqsClient - client to service methods on Amazon SQS 
	 * @param queueName - the name of the queue
	 */
	public SqsQueueService(AmazonSQSClient sqsClient, String queueName) {
		this.sqsClient = sqsClient;
		this.sqsClient.setRegion(Region.getRegion(Regions.EU_CENTRAL_1));
		
		CreateQueueRequest createQueueRequest = new CreateQueueRequest(queueName);
        this.queue = this.sqsClient.createQueue(createQueueRequest).getQueueUrl();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * This implementation pushes a single message into an Amazon SQS queue. 
	 */
	@Override
	public void push(IMessage message) {
			System.out.println("Add: " + message.toString());
			SendMessageRequest messageRequest = new SendMessageRequest(
					this.queue, 
					ion.morozan.message.Message.encodeToBase64(message)
			);
			
			this.sqsClient.sendMessage(messageRequest);				
	}
	
	/**
	 * {@inheritDoc}
	 * 
	 * This implementation receives a single message from the Amazon SQS queue.
	 */
	@Override
	public IMessage pull() throws InterruptedException {
		
		ReceiveMessageRequest receiveMessageRequest = new ReceiveMessageRequest(this.queue);
        List<Message> sqsMessages = this.sqsClient.receiveMessage(receiveMessageRequest).getMessages();
        
        // Quick polling to the Amazon SQS queue can possibly return 0 messages
        // To this end if the queue is empty we wait some time until we retry.
		if (sqsMessages.isEmpty()){
			System.out.println("Nothing in the queue. " + Thread.currentThread().getName() + " will wait and try again...");
			Thread.sleep(1500);
			return null;
		}
		
		return getVisibleMessage(sqsMessages.get(0));
	}
	
	/**
	 * {@inheritDoc}
	 * 
	 * This implementation deletes a received message from the Amazon SQS queue.
	 */	
	@Override
	public void delete(IMessage message) {
		System.out.println("Remove: " + message.toString() + " from queue.");
		String messageReceiptHandle = message.getId().toString();
        this.sqsClient.deleteMessage(new DeleteMessageRequest(this.queue, messageReceiptHandle));
	}

	/**
	 * Get a visible message from the SQS queue and set a random invisibility timeout.
	 * 
	 * @param sqsMessage - sqs message to be processed
	 * @return an instance of the local message
	 */
	private IMessage getVisibleMessage(Message sqsMessage ){
		IMessage message = null;
		
		// Set a random timeout in seconds and update the sqs object
		int timeOut = ThreadLocalRandom.current().nextInt(ion.morozan.message.Message.MAX_TIMEOUT);
		ChangeMessageVisibilityRequest change = new ChangeMessageVisibilityRequest(
				this.queue, 
				sqsMessage.getReceiptHandle(),
				timeOut);
		this.sqsClient.changeMessageVisibility(change);
		
		// Try deserialize the message and set the id of the message to the sqs generated id.
		// This is done to ease the process the deletion process.
		message =  ion.morozan.message.Message.decodeFromBase64(sqsMessage.getBody());
		
		String sqsMessageHandler = sqsMessage.getReceiptHandle();
		System.out.println("\t[Info][Read]: Message " + message.getId() 
				+ " changes its id to : " + sqsMessageHandler.substring(sqsMessageHandler.length() -36) 
				+ ". Now message is invisibile for: " + timeOut + "s");
		
		message.setId(sqsMessage.getReceiptHandle());
		
		return message;
	}
}