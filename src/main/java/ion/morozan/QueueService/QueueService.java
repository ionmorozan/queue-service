package ion.morozan.queueservice;

import ion.morozan.message.IMessage;

/** 
 * Message queue interface meant to be support a producer consumer scenario.
 * 
 * @author Ion Morozan
 */
public interface QueueService{
	
	/**
	 * Pushes a message on to queue.
	 * 
	 * @param message - the message
	 */
	public void push(IMessage message);
	
	/**
	 * Receives a single message from a queue if visible.
	 * 
	 * @return the message if visible or null
	 * @throws InterruptedException when thread is interrupted
	 */
	public IMessage pull() throws InterruptedException;
	
	/**
	 * Deletes a received message from the queue.
	 * 
	 * @param message - message to be deleted
	 */
	public void delete(IMessage message);
}
