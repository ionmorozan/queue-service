package ion.morozan.queueservice;

import ion.morozan.message.IMessage;
import ion.morozan.message.Message;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.lang.Object;

/**
 * File based message queue service.
 * 
 * @author Ion Morozan
 */
public class FileQueueService implements QueueService{

	/**
	 * File based queue
	 */
	private File queue;
	
	/**
	 * Handler to manipulate the queue in memory
	 */
	private FileChannel queueHandler;
	
	/**
	 * Lock to control access to the shared queue
	 */
	private final Lock lock;
	
	/**
	 * Conditional to wait for new messages if the queue is empty
	 */
	private final Condition queueEmpty;
	
	/**
	 * Create an instance of the {@link File queue} class
	 * 
	 * @param queue - file handler
	 */
	public FileQueueService(File queue) {
		this.queue = queue;
		try {
			this.queueHandler = new RandomAccessFile(this.queue, "rw").getChannel();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		this.lock = new ReentrantLock();
		this.queueEmpty = lock.newCondition();
	}
	
	/**
	 * {@inheritDoc}
	 * 
	 * This implementation pushes a single message into an file based queue. 
	 */
	@Override
	public void push(IMessage message) {
		lock.lock();
		try{
			addMessageToQueue(message);
			queueEmpty.signalAll();
		}finally{
			lock.unlock();	
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * This implementation receives a single message from the a file based queue.
	 */
	@Override
	public IMessage pull() throws InterruptedException {
		lock.lock();
		try{
			if (isQueueEmpty()){
				System.out.println("Nothing in the queue. " + Thread.currentThread().getName() + " is waiting...");
				queueEmpty.await();
				System.out.println("New messages added by: " + Thread.currentThread().getName());
			}
			
			return tryGetMessageFromQueue();
			
		} finally{
			lock.unlock();
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * This implementation deletes a received message from the a file based queue.
	 */	
	@Override
	public void delete(IMessage msg) {
		lock.lock();
		try{
			if (isQueueEmpty()){
				return;
			}
			System.out.println("Remove: " + msg.toString());
			removeMessageFromQueue(msg);
		}finally{
			lock.unlock();
		}
	}
	
	/**
	 * Verifies if the queue is empty.
	 * 
	 * @return true if the queue is empty, false otherwise
	 */
	public boolean isQueueEmpty(){
		return this.queue.length() == 0;
	}
	
	/**
	 * Add a message to the queue.
	 * 
	 * @param message - information to be added to the queue.
	 */
	private void addMessageToQueue(IMessage message){
		try {
			byte[] messageStream = message.serialize();
			int message_size = messageStream.length;
			long end_of_queue = this.queue.length();
			
			System.out.println("Add: " + message.toString() + " with size: " + message_size + " bytes");
			MappedByteBuffer map = this.queueHandler.map(
														 FileChannel.MapMode.READ_WRITE, 
														 end_of_queue, 
														 message_size + Integer.BYTES
														 );
			
			// Add the <MESSAGE_SIZE, Message> tuple to the queue
			map.putInt(message_size);
			map.put(messageStream);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Remove a message from queue.
	 * 
	 * @param message - message to be removed.
	 * @return true if the message was found, false otherwise 
	 */
	private boolean removeMessageFromQueue(IMessage message){
		boolean result = false;
		
		// A tuple containing <message, message_size, newMemoryMappedRegion, queuePosition>
		Object[] nextMessageInfo;
		IMessage tmpMessage = null;
		int message_size = 0;
		MappedByteBuffer map = null;
		long currentQueuePosition = 0;
		
		// While we did not reach the end of the queue read the next message
		while(currentQueuePosition < this.queue.length()){
			nextMessageInfo = readNextMessageFromQueue(currentQueuePosition);
			
			// Get content of the current messageInfo
			tmpMessage = (IMessage)nextMessageInfo[0];
			message_size = (int)nextMessageInfo[1];
			map = (MappedByteBuffer)nextMessageInfo[2];
			currentQueuePosition = (long)nextMessageInfo[3];
			
			// If the message is found, remove it from the queue by suppressing it with 0 values
			if (tmpMessage != null && tmpMessage.getId().compareTo(message.getId()) == 0){
				
				// Create a zero based bytes array and write it to queue instead of the message
				byte[] bytes = new byte[message_size];
				
				map.reset();
				map.putInt(message_size);
				map.put(bytes, 0, message_size);
				System.out.println("\t[Info][Remove] Found " + tmpMessage.toString());
				
				result = true;
				break;
			}
		}			

		return result; 
	}

	/**
	 * Try to get a message from the queue if visible.
	 * 
	 * @return the message if visible null otherwise
	 */
	private IMessage tryGetMessageFromQueue(){
		IMessage message = null;
		
		// A tuple containing <message, message_size, newMemoryMappedRegion, queuePosition>
		Object[] nextMessageInfo;
		IMessage tmpMessage = null;
		int message_size = 0;
		MappedByteBuffer map = null;
		long currentQueuePosition = 0;
		
		// While we did not reach the end of the queue read the next message
		while(currentQueuePosition < this.queue.length()){
			nextMessageInfo = readNextMessageFromQueue(currentQueuePosition);
		
			// Get the content of the current messageInfo
			tmpMessage = (IMessage)nextMessageInfo[0];
			message_size = (int)nextMessageInfo[1];
			map = (MappedByteBuffer)nextMessageInfo[2];
			currentQueuePosition = (long)nextMessageInfo[3];
			
			// If the message is visible, make it invisible for a random time.
			if (tmpMessage != null && tmpMessage.isVisible()){
				tmpMessage.setRandomVisibilityTimeout();
				map.reset();
				map.putInt(message_size);
				map.put(tmpMessage.serialize(), 0, message_size);
				message = tmpMessage;
				
				System.out.println("\t[Info][Read]: Found visible " + tmpMessage.toString());

				break;
			}
		}			
	 
		return message; 
	}

	/**
	 * Read the next Message from the queue (file).
	 *  
	 * @param currentQueuePosition - current position in the queue
	 * @return a tuple containing (message, message_size, newMemoryMappedRegion, queuePosition)
	 */
	private Object[] readNextMessageFromQueue(long currentQueuePosition){
		IMessage nextMessage = null;
		int message_size = 0;
		MappedByteBuffer map = null;
		
		try {
			byte[] messageStream = null;
			
			// If we did not reach the end of the queue
			if(currentQueuePosition < this.queue.length()){
				// Get the size of the next message
				map = this.queueHandler.map(FileChannel.MapMode.READ_WRITE, currentQueuePosition, Integer.BYTES);
				message_size = map.getInt();
				messageStream = new byte[message_size];
			
				// Read the message and mark its position in the queue
				map = this.queueHandler.map(FileChannel.MapMode.READ_WRITE, currentQueuePosition, Integer.BYTES + message_size);
				map.mark();
				map.getInt(); // noop operation
				map.get(messageStream);
				
				nextMessage = Message.deserialize(messageStream);
				
				// Advance the position in the queue
				currentQueuePosition += message_size + Integer.BYTES;
			}			
		} catch (IOException e){
			e.printStackTrace();
		} 
		
		return new Object[]{nextMessage, message_size, map, currentQueuePosition}; 
	}
}