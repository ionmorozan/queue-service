package ion.morozan.queueservice;

import ion.morozan.message.IMessage;

import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * In memory message queue service.
 *  
 * @author Ion Morozan
 */
public class InMemoryQueueService implements QueueService {
	
	/**
	 * Queue of messages 
	 */
	private Queue<IMessage> queue;

	/**
	 * Lock to control access to the shared queue
	 */
	private final Lock lock;
	
	/**
	 * Conditional to wait for new messages if the queue is empty
	 */
	private final Condition queueEmpty; 

	public InMemoryQueueService(){
		this.queue = new LinkedList<IMessage>();
		this.lock = new ReentrantLock();
		this.queueEmpty = this.lock.newCondition();
	}
	
	/**
	 * {@inheritDoc}
	 * 
	 * This implementation pushes a single message into an in-memory queue. 
	 */
	@Override
	public void push(IMessage message) {
		lock.lock();
		try{
			System.out.println("Add: " + message.toString());
			this.queue.add(message);
			queueEmpty.signalAll();
		}finally{
			lock.unlock();	
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * This implementation receives a single message from the in-memory queue 
	 */
	@Override
	public IMessage pull() throws InterruptedException {
		lock.lock();
		try{
			if (this.queue.isEmpty()){
				System.out.println("Nothing in the queue. " + Thread.currentThread().getName() + " is waiting...");
				queueEmpty.await();
				System.out.println("New messages added by: " + Thread.currentThread().getName());
			}
			
			return this.TryGetVisibleMessage(); 
			
		}finally{
			lock.unlock();
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * This implementation deletes a received message from the in-memory queue 
	 */	
	@Override
	public void delete(IMessage message) {
		lock.lock();
		try{
			if (this.queue.isEmpty()){
				return;
			}
			System.out.println("Remove: " + message.toString() + " from queue." );
			this.queue.remove(message);
		}finally{
			lock.unlock();
		}
		
	}

	/**
	 * Get the current size of the queue
	 * 
	 * @return the size of the queue
	 */
	public int getQueueSize(){
		return this.queue.size();
	}
	
	/**
	 * Try get a visible message from the queue if available and set a 
	 * random timeout in milliseconds where the message is not visible 
	 * in the queue.
	 * Once the timeout expires the message becomes available at the head 
	 * of the queue.
	 * 
	 * @return the message from the queue or null if none available
	 */
	private IMessage TryGetVisibleMessage(){
		for (IMessage message : queue) {
			if (message.isVisible()){
				message.setRandomVisibilityTimeout();
				System.out.println("\t[Info][Read] Found visible " + message.toString());
				
				return message;
			}
		}
		
		return null;
	}
}