package ion.morozan.demo;

import ion.morozan.message.IMessage;
import ion.morozan.queueservice.QueueService;

import java.util.Random;
import java.util.concurrent.Callable;

/**
 * Class that consumes a message from a queue.
 * 
 * @author Ion Morozan
 */
public class Consumer implements Callable<Void>{

	/**
	 * Queue to consume messages from.
	 */
	private QueueService queueService;
	
	/**
	 * Get an instance of the {@link Consumer} class.
	 * 
	 * @param queueService - queue to consume messages from
	 */
	Consumer(QueueService queueService) {
		this.queueService = queueService; 
	}
	
	/**
	 * Consume messages from the queue and randomly removes it from the queue.
	 * 
	 * @throws InterruptedException when a thread is terminated unexpectedly
	 */
	@Override
	public Void call() throws InterruptedException {
		while (!Thread.interrupted()) {
			// Get a message from the queue
			IMessage message = this.queueService.pull();
			
			// If there is a visible message, consume it and possibly delete it
			if (message != null){
				this.consume(message);
			}
			Thread.sleep(1000);
		}
		
		return null;
	}
	
	/**
	 * Consume the message and randomly deletes it from the queue.
	 * 
	 * @param message - resource to be consumed
	 */
	private void consume(IMessage message) { 
		
		System.out.println("Consume: " + message.toString());
		
		boolean removeMessageFromQeueue = new Random().nextBoolean();
		if (removeMessageFromQeueue){
			this.queueService.delete(message);
		}
	}
}