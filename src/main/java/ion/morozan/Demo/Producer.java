package ion.morozan.demo;

import ion.morozan.message.IMessage;
import ion.morozan.message.Message;
import ion.morozan.queueservice.QueueService;

import java.util.concurrent.Callable;

/**
 * Class that produces messages and add it to the queue.
 * 
 * @author Ion Morozan
 */
public class Producer implements Callable<Void>{

	/**
	 * Queue to push the messages to
	 */
	QueueService queueService;
	
	/**
	 * Get an instance of the {@link Producer} class.
	 * 
	 * @param queueService - queue to push the messages to
	 */
	public Producer(QueueService queueService) {
		this.queueService = queueService;
	} 
	
	/**
	 * Create a random message and pushes it to a queue
	 * @throws InterruptedException when a thread is terminated unexpectedly
	 */
	@Override 
	public Void call() throws InterruptedException { 
		while (!Thread.interrupted()) {
			this.queueService.push(this.produce());
			Thread.sleep(1500);
		}
		
		return null;
	}
	
	/**
	 * Produce a random IMessage.
	 * 
	 * @return a new random message
	 */
	private IMessage produce(){
		IMessage message = new Message();
		
		return message.createMessage();
	}
}