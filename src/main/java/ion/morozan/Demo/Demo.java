package ion.morozan.demo;

import ion.morozan.queueservice.FileQueueService;
import ion.morozan.queueservice.InMemoryQueueService;
import ion.morozan.queueservice.QueueService;
import ion.morozan.queueservice.SqsQueueService;

import java.io.File;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import com.amazonaws.AmazonClientException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.services.sqs.AmazonSQSClient;

/**
 * This class is a show-case of the 3 implementations of the queue service.
 * 
 * @author Ion Morozan
 */
public class Demo {

	/**
	 * How many seconds the demo should run in seconds.
	 */
	private static final int RUNNING_TIME = 30;
	
	public static void main(String[] args){
		// By default run the in-memory queue service
		byte scenario = 0;
		
		if(args.length > 0) {
			try {
				scenario = Byte.parseByte(args[0]);
			} catch (NumberFormatException e) {
				System.out.println("[HELP] There is no such scenario. Try one of the following: \n"
						+ " 0 - to startInMemoryQueueServiceDemo \n"
						+ " 1 - to startFileQueueServiceDemo \n"
						+ " 2 - startSqsAmazonQueueServiceDemo");
				System.out.println("Starting with default scenario - startInMemoryQueueServiceDemo \n\n");
				// Ignore exception on purpose.
		      }
		}
		
		switch (scenario) {
			case 0: startInMemoryQueueServiceDemo();
					break;
			case 1: startFileQueueServiceDemo();
					break;
			case 2: startSqsAmazonQueueServiceDemo();
					break;
		};
	}
	
	/**
	 * Starts the in memory queue service implementation.
	 */
	private static void startInMemoryQueueServiceDemo(){
		QueueService inMemoryQueueService = new InMemoryQueueService();
		startDemo(inMemoryQueueService);	
	}
	
	/**
	 * Starts the file based queue service implementation.
	 */
	private static void startFileQueueServiceDemo(){
		File queueLocation = new File("db.txt");
		QueueService fileQueueService = new FileQueueService(queueLocation);
		startDemo(fileQueueService);
	}
	
	/**
	 * Starts the SQS Amazon queue service implementation.
	 */
	private static void startSqsAmazonQueueServiceDemo(){
		AWSCredentials credentials = null;
        try {
            credentials = new ProfileCredentialsProvider().getCredentials();
        } catch (Exception e) {
            throw new AmazonClientException(
                    "Cannot load the credentials from the credential profiles file. " +
                    "Please make sure that your credentials file is at the correct " +
                    "location (~/.aws/credentials), and is in valid format.",
                    e);
        }
		
        AmazonSQSClient sqs = new AmazonSQSClient(credentials); 
		QueueService sqsQueuService = new SqsQueueService(sqs, "Queue");
		startDemo(sqsQueuService);
	}
	
	/**
	 * Start the demo for a particular queue service.
	 * 
	 * @param queueService - particular queue service implementation
	 */
	private static void startDemo(QueueService queueService){
	
		ExecutorService executor = Executors.newFixedThreadPool(3);
        Future<Void> producer = executor.submit(new Producer(queueService));
        Future<Void> consumer1 = executor.submit(new Consumer(queueService));
        Future<Void> consumer2 = executor.submit(new Consumer(queueService));

        try {
            System.out.println("Demo starts with 1 Producer and 2 Consumers. It will run for : " + Demo.RUNNING_TIME +"s");
            producer.get(Demo.RUNNING_TIME, TimeUnit.SECONDS);
            consumer1.get(Demo.RUNNING_TIME, TimeUnit.SECONDS);
            consumer2.get(Demo.RUNNING_TIME, TimeUnit.SECONDS);
				
            System.out.println("Time has exipred. Going to cancel the execution");
        } catch (InterruptedException | ExecutionException e) {
        	// Exception expected when forcing a thread to finish - ignore
        } catch (TimeoutException e) {
            producer.cancel(true);
            consumer1.cancel(true);
            consumer2.cancel(true);
            
            System.out.println("Demo Finished.");
        }
       
        executor.shutdownNow();
	}
}